package com.grayroutesgmapskt.model.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface MapsLocationDao {
    @Insert
    fun insertData(mapsTable: MapsTable): Long

    @Query("SELECT * FROM mapsTable")
    fun getAllLocation(): List<MapsTable>
}