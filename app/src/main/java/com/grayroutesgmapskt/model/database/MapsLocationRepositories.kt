package com.grayroutesgmapskt.model.database

import android.app.Application

class MapsLocationRepositories(application: Application) {

    private var mapsLocationDao: MapsLocationDao?

    init {
        val db = MapsRoomDb.getDatabase(application.applicationContext)
        mapsLocationDao = db?.mapsDao()
    }

    suspend fun insertData(mapsTable: MapsTable) {
        mapsLocationDao?.insertData(mapsTable)
    }

    suspend fun getAllLocation(): List<MapsTable> {
        return mapsLocationDao!!.getAllLocation()
    }
}