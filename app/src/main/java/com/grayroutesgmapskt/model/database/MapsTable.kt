package com.grayroutesgmapskt.model.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "mapsTable")
data class MapsTable(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Int,
    @ColumnInfo(name = "latitude") var latitude: String?,
    @ColumnInfo(name = "longitude") var longitude: String?,
    @ColumnInfo(name = "address") var address: String?,
    @ColumnInfo(name = "userImage") var userImage: ByteArray?
)