package com.grayroutesgmapskt.model.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [MapsTable::class], version = 1)
abstract class MapsRoomDb : RoomDatabase() {
    abstract fun mapsDao(): MapsLocationDao
    companion object {
        var INSTANCE: MapsRoomDb? = null
        fun getDatabase(mContext: Context): MapsRoomDb? {
            if (INSTANCE == null) {
                synchronized(MapsRoomDb::class) {
                    INSTANCE =
                        Room.databaseBuilder(
                            mContext.applicationContext,
                            MapsRoomDb::class.java,
                            "MapsDb"
                        ).build()
                }
            }
            return INSTANCE
        }
        fun destroyDB() {
            INSTANCE = null
        }
    }
}