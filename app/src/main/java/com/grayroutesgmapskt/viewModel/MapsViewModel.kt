package com.grayroutesgmapskt.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.grayroutesgmapskt.model.database.MapsLocationRepositories
import com.grayroutesgmapskt.model.database.MapsTable

class MapsViewModel(application: Application) : AndroidViewModel(application) {

    private var mapsLocationRepositories = MapsLocationRepositories(application)

    suspend fun insertLocation(mapsTable: MapsTable) {
        mapsLocationRepositories.insertData(mapsTable)
    }

    suspend fun getAllLocation(): List<MapsTable> {
        return mapsLocationRepositories.getAllLocation()
    }
}