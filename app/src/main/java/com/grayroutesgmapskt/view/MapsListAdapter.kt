package com.grayroutesgmapskt.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.grayroutesgmapskt.R
import com.grayroutesgmapskt.databinding.ListItemLauoutBinding
import com.grayroutesgmapskt.model.database.MapsTable
import java.text.DecimalFormat

class MapsListAdapter(private val mContext: Context, private val list: List<MapsTable>) :
    RecyclerView.Adapter<MapsListAdapter.LocationListViewHolder>() {
    private val decimalFormat = DecimalFormat("#,##0.##")
    var mapIntent = Intent(mContext, MapsActivity::class.java)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationListViewHolder {
        var listItemLayoutBinding: ListItemLauoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_lauout,
            parent,
            false
        )
        return LocationListViewHolder(listItemLayoutBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: LocationListViewHolder, position: Int) {
        try {
            holder.itemListLayoutBinding.tvAddress.text = list[position].address
            val lat: String = decimalFormat.format(list[position].latitude?.toDouble())
            val long1: String =
                decimalFormat.format(list[position].longitude?.toDouble())
            holder.itemListLayoutBinding.tvLatLong.text =
                "Lat: $lat Long: $long1"
            var bm = BitmapFactory.decodeByteArray(
                list[position].userImage,
                0,
                list[position].userImage!!.size
            )
            holder.itemListLayoutBinding.ivUserImage.setImageBitmap(bm)
            holder.itemListLayoutBinding.llItem.setOnClickListener {
                var mapsItem = list[position]
                if (mapsItem == null) {
                    Toast.makeText(mContext, "Try Again ", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
                mapIntent.putExtra("LATITUDE", mapsItem.latitude)
                mapIntent.putExtra("LONGITUDE", mapsItem.longitude)
                mapIntent.putExtra("ADDRESS", mapsItem.address)
                mContext.startActivity(mapIntent)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(mContext, "Some Error Occured", Toast.LENGTH_SHORT).show()
            Log.e("Log -- >", "onBindViewHolder: " + e)
        }
    }

    class LocationListViewHolder(itemLayoutBinding: ListItemLauoutBinding) :
        RecyclerView.ViewHolder(itemLayoutBinding.root) {
        var itemListLayoutBinding = itemLayoutBinding
    }
}

