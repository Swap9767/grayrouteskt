package com.grayroutesgmapskt.view

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.grayroutesgmapskt.R
import com.grayroutesgmapskt.databinding.ActivityListBinding
import com.grayroutesgmapskt.model.database.MapsTable
import com.grayroutesgmapskt.viewModel.MapsViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream


class ListActivity : AppCompatActivity() {

    private lateinit var mContext: Context
    private lateinit var activityListBinding: ActivityListBinding
    private lateinit var mapsViewModel: MapsViewModel
    private lateinit var listAdapter: MapsListAdapter
    private lateinit var list: ArrayList<MapsTable>
    private var latitude = ""
    private var longitude = ""
    private var address = ""
    private val CAMERA_PERMISSIONCODE = 1001
    private val REQ_CAPTURE_IMAGE = 1002
    private lateinit var temp: MapsTable

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = this
        activityListBinding = DataBindingUtil.setContentView(this, R.layout.activity_list)
        mapsViewModel = ViewModelProviders.of(this).get(MapsViewModel::class.java)
        initView()
    }

    override fun onResume() {
        super.onResume()
        mContext = this
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun initView() {
        try {
            list = ArrayList<MapsTable>()
            if (intent.extras != null) {
                var bundle: Bundle? = intent.extras
                latitude = bundle?.getString("LATITUDE").toString()
                longitude = bundle?.getString("LONGITUDE").toString()
                address = bundle?.getString("ADDRESS").toString()
            }
            CoroutineScope(IO).launch {
                list = mapsViewModel.getAllLocation() as ArrayList<MapsTable>
                var isAlreadyAdded = false
                if (list != null || list.isNotEmpty()) {
                    var i = 0
                    while (i < list.size) {
                        if (list[i].longitude.equals(longitude) && list[i].latitude.equals(
                                latitude
                            )
                        ) {
                            temp = list[i]
                            isAlreadyAdded = true
                        }
                        i++
                    }
                }
                Log.d("Log -->", "isAlreadyAdded: $isAlreadyAdded")
                if (!isAlreadyAdded) {
                    withContext(Main) {
                        captureImage()
                    }
                } else {
                    val t = temp
                    list.remove(t)
                    list.add(0, temp)
                }
                withContext(Main) {
                    activityListBinding.rvList.layoutManager =
                        LinearLayoutManager(mContext, RecyclerView.VERTICAL, false)
                    listAdapter = MapsListAdapter(mContext, list)
                    activityListBinding.rvList.adapter = listAdapter
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(mContext, "Some Error occured", Toast.LENGTH_SHORT).show()
            Log.e("Log -->", "initView: " + e)
        }
    }

    private fun captureImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                    arrayOf(Manifest.permission.CAMERA),
                    CAMERA_PERMISSIONCODE
                )
                return
            }
        }
        startActivityForResult(Intent(MediaStore.ACTION_IMAGE_CAPTURE), REQ_CAPTURE_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQ_CAPTURE_IMAGE -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    var bm = data.extras?.get("data") as Bitmap
                    val bos = ByteArrayOutputStream()
                    bm.compress(Bitmap.CompressFormat.PNG, 100, bos)
                    val bArray = bos.toByteArray()
                    saveData(bArray)
                }
            }
        }
    }

    private fun saveData(bArray: ByteArray) {
        var mapTable = MapsTable(0, latitude, longitude, address, bArray)
        CoroutineScope(IO).launch {
            mapsViewModel.insertLocation(mapTable)
            list.add(0, mapTable)
            withContext(Main) {
                listAdapter.notifyDataSetChanged()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_PERMISSIONCODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivityForResult(
                    Intent(MediaStore.ACTION_IMAGE_CAPTURE),
                    REQ_CAPTURE_IMAGE
                )
            } else {
                Toast.makeText(this, "CAMERA_PERMISSION_DENIED", Toast.LENGTH_LONG).show()
            }
        }
    }
}