package com.grayroutesgmapskt.view

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.grayroutesgmapskt.R
import com.grayroutesgmapskt.viewModel.MapsViewModel
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var mContext: Context
    private var latitude = ""
    private var longitude = ""
    private var address = ""
    private lateinit var mapIntent: Intent
    private lateinit var mapsViewModel: MapsViewModel
    private val REQUEST_LOCATION = 201

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        mContext = this
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        checkPermission()
        mapFragment.getMapAsync(this)
        mapsViewModel = ViewModelProviders.of(this).get(MapsViewModel::class.java)
    }

    private fun checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ), REQUEST_LOCATION
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            REQUEST_LOCATION -> {
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(
                        this,
                        "PERMISSION_DENIED Please accept permission",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }


    override fun onResume() {
        super.onResume()
        mContext = this
        try {
            if (intent.extras != null) {
                var bundle: Bundle? = intent.extras
                latitude = bundle?.getString("LATITUDE").toString()
                longitude = bundle?.getString("LONGITUDE").toString()
                address = bundle?.getString("ADDRESS").toString()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        CoroutineScope(IO).launch {
            var list = mapsViewModel.getAllLocation()
            if (list.isNotEmpty()) {
                list.forEach {
                    if (!TextUtils.isEmpty(latitude)) {
                        val userImageByteArray: ByteArray = it.userImage!!
                        val bm = BitmapFactory.decodeByteArray(
                            userImageByteArray,
                            0,
                            userImageByteArray.size
                        )
                        withContext(Main) {
                            var latLong = LatLng(
                                it.latitude!!.toDouble(),
                                it.longitude!!.toDouble()
                            )
                            if (it.latitude == latitude && it.longitude == longitude) {
                                mMap.moveCamera(
                                    CameraUpdateFactory.newLatLng(
                                        latLong
                                    )
                                )
                                mMap.animateCamera(CameraUpdateFactory.zoomTo(10f))
                            }
                            mMap.addMarker(
                                MarkerOptions().position(latLong).title(address).icon(
                                    BitmapDescriptorFactory.fromBitmap(
                                        createCustomMarker(
                                            mContext,
                                            bm
                                        )
                                    )
                                )
                            )
                        }
                    }
                }
            }
        }
        mMap.setOnMapLongClickListener { latLong ->
            try {
                address = Geocoder(mContext, Locale.getDefault()).getFromLocation(
                    latLong.latitude,
                    latLong.longitude,
                    1
                )[0].getAddressLine(0)
                if (TextUtils.isEmpty(address)) {
                    Toast.makeText(
                        mContext,
                        "Address is Empty , Use Different Location",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                    return@setOnMapLongClickListener
                }
                mMap.addMarker(
                    MarkerOptions().position(latLong).title(address)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                )
                mMap.moveCamera(CameraUpdateFactory.zoomTo(10f))
                latitude = latLong.latitude.toString()
                longitude = latLong.longitude.toString()
                mapIntent = Intent(this, ListActivity::class.java)
                mapIntent.putExtra("LATITUDE", latitude)
                mapIntent.putExtra("LONGITUDE", longitude)
                mapIntent.putExtra("ADDRESS", address)
                startActivity(mapIntent)
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("Log -- > ", "onMapLongClick: $e")
                Toast.makeText(this, "Some Error occured", Toast.LENGTH_SHORT).show()
            }
        }
        mMap.setOnMarkerClickListener { marker ->
            latitude = marker.position.latitude.toString()
            longitude = marker.position.longitude.toString()
            address = marker.title
            mapIntent = Intent(this, ListActivity::class.java)
            mapIntent.putExtra("LATITUDE", latitude)
            mapIntent.putExtra("LONGITUDE", longitude)
            mapIntent.putExtra("ADDRESS", address)
            startActivity(mapIntent)
            return@setOnMarkerClickListener false
        }
    }

    private fun createCustomMarker(context: Context, bm: Bitmap?): Bitmap? {
        val marker: View =
            (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                R.layout.markericon_layout,
                null
            )
        val markerImage = marker.findViewById(R.id.user_dp) as CircleImageView
        markerImage.setImageBitmap(bm)
        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        marker.layoutParams = ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT)
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
        marker.buildDrawingCache()
        val bitmap = Bitmap.createBitmap(
            marker.measuredWidth,
            marker.measuredHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        marker.draw(canvas)
        return bitmap
    }

    override fun onBackPressed() {
        Snackbar.make(window.decorView, "Do You want to close app?", Snackbar.LENGTH_LONG)
            .setAction("Yes") { finishAffinity() }.show()
    }
}